From ubuntu:14.04
Maintainer Mingwei Zhang
RUN apt-get update && \
    apt-get install -y python \
                       python-pip \
                       imvirt \
                       binutils \
    && pip install pygeoip
COPY . /scout/
WORKDIR /scout
ENTRYPOINT ["sleep", "infinity"]
