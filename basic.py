#!/usr/bin/python
from __future__ import print_function
import sys
import os
import json
import subprocess

global_root_dir = "/"
def global_init():
    global global_root_dir
    global_root_dir = "/"

def get_global_root():
    return global_root_dir

def set_global_root(path):
    global global_root_dir
    global_root_dir = path

class Global_Config(object):

    instance = None

    @classmethod
    def get(self):
        return Global_Config.instance

    def __init__(self):
        self.config = dict()
        file='./config/basic.conf'
        self.json_handler = open(file)
        try:
            self.config = json.load(self.json_handler)
        except ValueError as err:
            print("config file: [%s] is not in JSON format"%file)
            print(err)
            sys.exit(1)
        Global_Config.instance = self


def get_config():
    config_instance = Global_Config.get()
    return config_instance.config

class Debug_Print(object):
    def __init__(self):
        self._stdout = sys.stdout
    def write(self,text):
        if(get_config()['debug'] == "False"):
            return None
        if isinstance(text, basestring):
            text = text.rstrip()
        if text:
            self._stdout.write('Vanguard~~~> {0}\n'.format(text))
            return text
    __call__ = write

global debug_print
debug_print = Debug_Print()

class Basic_Object(object):
    def __init__(self, name):
        self.name = name
        self.data = None
    def run(self):
        pass
    def send_info(self, recon_info):
        recon_info[self.getname()] = self.data
        pass

    def getname(self):
        return self.name

    def getdata(self):
        return self.data

    def get_data_elem(self, index):
        if(self.data == None):
            return ""
        if(index < 0):
            return ""
        if(isinstance(self.data, list)):
            if (len(self.data) > index):
                return self.data[index]
            else:
                return ""
        else:
            if (index == 0):
                return self.data
            else:
                return ""

# if you want to run a script and the output is only
# one element, ie. one line of string (or other characters)
# then use this object
class Script_Object(Basic_Object):
    def __init__(self, name, script_cmd, param=[]):
        self.name = name
        self.data = None
        param = ' '.join(param)
        self.script_cmd = script_cmd + ' ' + param

    def postprocess(self, data):
        return data.strip()
    def run(self):
        self.data = os.popen(self.script_cmd).read()
        self.data = self.postprocess(self.data)

#======================================================
class Data_Type(object):
    raw_data = 0
    table=1
    keyvalue=2

class Data_Script(Basic_Object):
    def __init__(self, interp, data_type, sep, cmd, preprocessor, name):
        self.data=None
        self.data_type = data_type
        self.cmd = cmd
        self.sep =sep
        self.interp = interp
        self.preprocessor = preprocessor
        self.name = name
        self.print_value = False
        pass
    def popen(self, interp, cmd):
        cmd = interp + ' ' + cmd
        return subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)

    def setup_print():
        self.print_value = True

    def run(self):
        #debug_print(self.interp)
        proc = self.popen(self.interp, self.cmd)
        self.process_meta_line(proc)
        while True:
            line = proc.stdout.readline()
            if line != '':
                line = line.strip()
                line = line.rstrip()
                self.process_data_line(line)
            else:
                break
        if(self.print_value):
            print(self.data)

    def split(self, line):
        if(self.sep == None):
            list = line.split()
        else:
            list = line.split(self.sep)
        if(len(list) == 1):
            return (list[0], "")
        else:
            return list

    def process_meta_line(self, proc):
        pass

    def process_data_line(self, line):
        pass

class List_Script(Data_Script):
    def process_data_line(self, line):
       if(self.data == None):
           self.data = []
       self.data.append(line)

# if you want to run a script and the output is a table, then use this object.
class Table_Script(Data_Script):
    def __init__(self, interp, sep, cmd, preprocessor, name, attributes = None):
        super(Table_Script, self).__init__(interp, Data_Type.table, sep, cmd, preprocessor, name)
        self.attributes = attributes
        self.data=[]

    def process_meta_line(self, proc):
        if(self.attributes != None):
            return
        self.attributes = self.split(proc.stdout.readline().rstrip())
        #print(self.attributes)

        if(self.preprocessor != None):
            self.attributes = self.preprocessor.process_attr_line(self.attributes)
            debug_print("attributes:")
            debug_print(self.attributes)

    def process_data_line(self, line):
        data = self.split(line)
        debug_print("data :%s"%line)
        if(self.preprocessor != None):
            data = self.preprocessor.process_attr_line(data)
        elem = dict()
        self.assign_element(elem, self.attributes, data)
        self.data.append(elem)

    def assign_element(self, elem, attrs, data):
        for i in range(len(attrs)):
            elem[attrs[i]] = data[i]


# if you want to run a script and the output is a list of
# key and value pairs, then use this object.
class Keyvalue_Script(Data_Script):
    def __init__(self, interp, sep, cmd, preprocessor, name):
        super(Keyvalue_Script, self).__init__(interp, Data_Type.keyvalue, sep, cmd, preprocessor, name)
        self.data=dict()

    def process_data_line(self, line):
        keyvalue = self.split(line)
        #all data specific feature are handled in preprocess function
        pp = self.preprocessor
        if(pp != None):
            (key, value) = pp.process_keyvalue(keyvalue[0], keyvalue[1])
        else:
            (key, value) = (keyvalue[0], keyvalue[1])
        self.data[key] = value


#=========================================================
class Collector(Basic_Object):
    def __init__(self):
        self.collectors = []

    def __init__(self, name):
        super(Collector, self).__init__(name)
        self.collectors = []

    def check_config(self, name, token):
        config = get_config()
        if(not 'scout_ctl' in config.keys()):
            print("malformed config, no scout_ctl")
            sys.abort()
        if(name in config['scout_ctl'].keys()):
            if(config['scout_ctl'][name].lower() == "true"):
                return True
            else:
                return False

        if(token != None):
            return token
        else:
            if(not 'scout_ctl_default' in config.keys()):
                return False
            else:
                if(config['scout_ctl_default'].lower() == "true"):
                    return True
                else:
                    return False

    def run(self, token=None):
        print("processing collector: %s"%self.getname())
        token = self.check_config(self.__class__.__name__.lower(), token)
        for col in self.collectors:
            if(isinstance(col, Collector)):
                col.run(token)
            else:
                #this is an end object
                if(self.check_config(col.__class__.__name__.lower(), token)):
                    col.run()


    def send_info(self, recon_info):
        recon_info[self.getname()] = dict()
        for col in self.collectors:
            col.send_info(recon_info[self.getname()])

class Super_Collector(Collector):
    def send_info(self, recon_info):
        for col in self.collectors:
            col.send_info(recon_info)

class Dynamic_Collector(Collector):
    def __init__(self, interp, data_type, sep, cmd, preprocessor, name):
        super(Dynamic_Collector, self).__init__(name)
        self.collector_list = List_Script(interp, data_type, sep, cmd, preprocessor, 'collector_list')
        self.collector_list.run()
        if(self.collector_list.data == None):
            return
        for v in self.collector_list.data:
            obj = self.init_collector(v)
            self.collectors.append(obj)

    def init_collector(v):
        pass

class Pattern_Checker(Basic_Object):
    def __init__(self, name):
        self.name = name
        self.methods = []
        self.data = None
        pass
    def append_check_method(self, cmd, match_strs, mismatch_strs):
        method = dict()
        method['cmd'] = cmd
        method['match'] = match_strs
        method['mismatch'] = mismatch_strs
        self.methods.append(method)
        pass
    def run(self):
        if(len(self.methods) == 0):
            return False
        for method in self.methods:
            debug_print("current method is %s"%method['cmd'])
            if(self.check_method(method)):
                self.data = True
                return True
        self.data = False
        return False

    def check_method(self, method):
        for mis in method['mismatch']:
            cmdline = method['cmd'] +" |grep -ci " + '"'+mis+'"'
            debug_print("cmdline: "+cmdline)
            output=os.popen(cmdline).read()
            #debug_print("output is %s"%output)
            outline=int(output)
            if(outline >0):
                return False

        for mat in method['match']:
            cmdline = method['cmd'] +" |grep -ci " + '"'+mat+'"'
            debug_print("cmdline: "+cmdline)
            output=os.popen(cmdline).read()
            #debug_print("output is %s"%output)
            outline=int(output)
            if(outline >0):
                return True

        #we don't know exactly if it is the provider, then we think it is not
        return False

class Returnvalue_Checker(Basic_Object):
    def __init__(self, name, script):
        self.name = name
        self.script_cmd = script
    def run(self, input_value):
        try:
            subprocess.check_call(self.script_cmd + " " + input_value, shell=True)
            return True
        except subprocess.CalledProcessError:
            debug_print("checking failed for input [%s]: calledprocessError"%input_value)
            return False
        except OSError:
            debug_print("checking failed for input [%s]: OsError"%input_value)
            return False

# Preprocessor allows user to trim format issues when collector is running
# It basically implements callback functions to handle each data element

class Preprocessor(Basic_Object):
    def __init__(self):
        pass
    def process_keyvalue(self, key, value):
        pass
    def process_attr_line(self, attrs):
        pass
