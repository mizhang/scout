#!/bin/bash
if [[ $# != 1 ]]; then
    echo "usage: ./build.sh <IMAGE_NAME>"
    exit 0
fi
IMAGE_NAME=$1
docker build -t $IMAGE_NAME .
