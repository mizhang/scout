#!/usr/bin/python
from basic import *
global global_root_dir
class Cloud_Provider_Checker(Collector):
    def __init__(self, name):
        super(Cloud_Provider_Checker, self).__init__(name)
        cloud_google = Pattern_Checker('Google')
        cloud_amazon = Pattern_Checker('Amazon')
        cloud_vmware = Pattern_Checker('Vmware')

        cmd_meta_aws="curl http://169.254.169.254/1.0/ 2>&1"
        cmd_meta="curl http://169.254.169.254/ 2>&1"
        cmd_dmi='cat '+get_global_root()+'sys/devices/virtual/dmi/id/* 2>/dev/null'
        cloud_google.append_check_method(cmd_dmi,["Google"], [])
        cloud_google.append_check_method(cmd_meta,["computeMetadata"], ["Failed to connect"])
        cloud_amazon.append_check_method(cmd_dmi,["Amazon"], [])
        cloud_amazon.append_check_method(cmd_meta_aws, ["meta-data"], ["Failed to connect"])
        cloud_vmware.append_check_method(cmd_dmi,["Vmware"], [])

        self.collectors.append(cloud_google)
        self.collectors.append(cloud_amazon)
        self.collectors.append(cloud_vmware)

    def send_info(self, recon_info):
        #super(Cloud_Provider_Checker, self).send_info(recon_info)
        for cloud in self.collectors:
            if(cloud.data == True):
                recon_info[self.getname()] = cloud.getname()
                return
        recon_info[self.getname()] = 'unknown'


class Virtualization_Checker(Script_Object):
    def postprocess(self, data):
        return data.strip();

class Cloud_Checker(Collector):
    def __init__(self):
        self.name = 'cloud_provider'
        self.collectors = []
        self.collectors.append(Cloud_Provider_Checker("provider_name"))
        self.collectors.append(Virtualization_Checker("virtualization", "imvirt"))
