#!/bin/bash
import re
import subprocess
from basic import *
class CGroup(Keyvalue_Script):
    pass
class Namespace(Keyvalue_Script):
    pass
class Capability(Script_Object):
    def send_info(self, recon_info):
        if(self.data == None or self.data == "" or re.match("No such", self.data)):
            recon_info[self.getname()] = ""
        else:
            recon_info[self.getname()] = dict()
            data_list = self.data.split('+')
            recon_info[self.getname()]['cap'] = data_list[0].split(',')
            if(len(data_list) == 1):
                recon_info[self.getname()]['set'] = ""
            else:
                recon_info[self.getname()]['set'] = data_list[1]

class Mountinfo(Table_Script):
    def __init__(self, interp, sep, cmd, preprocessor, name):
        attributes =['mount_id', 'parent_id', 'dev_no',
            'root', 'mount_point', 'mount_options', 'optional_fields',
            'fs_type', 'mount_source', 'super_options']
        super(Mountinfo, self).__init__(interp, sep, cmd, preprocessor, name, attributes)

    def process_data_line(self, line):
        prog = re.compile('(?P<mount_id>[0-9]+) (?P<parent_id>[0-9]+) ' '(?P<dev_no>[0-9]+:[0-9]+) (?P<root>\S+) (?P<mount_point>\S+) '
            '(?P<mount_options>\S+) (?P<optional_fields>[^-]*)- (?P<fs_type>\S+) '
            '(?P<mount_source>\S+)(?P<super_options>\s*\S*)$')
        m = prog.match(line)

        if(m != None):
            data =[m.group('mount_id'), m.group('parent_id'), m.group('dev_no'),
                m.group('root'), m.group('mount_point'),
                m.group('mount_options'), m.group('optional_fields'),
                m.group('fs_type'), m.group('mount_source'),
                m.group('super_options')]
            #print(data)
            elem = dict()
            self.assign_element(elem, self.attributes, data)
            self.data.append(elem)
        else:
            print("pattern does not match")

class Individual_Proc_Info(Collector):
    def __init__(self, name):
        super(Individual_Proc_Info, self).__init__(name)
        self.collectors.append(CGroup('', ":", 'cat ' + get_global_root() + 'proc/'+name+
            '/cgroup | sed \'s/^[0-9]\+://g\'', None, 'cgroup'))
        self.collectors.append(Namespace('',":", 'ls -l ' + get_global_root() + 'proc/'+name+
            '/ns 2>/dev/null| ' + 'cut -d\' \' -f 11- | sed \'s/\[//g; s/\]//g; /^\s*$/d\'', None, 'namespace'))
        self.collectors.append(Mountinfo('', " ", 'cat ' + get_global_root() + 'proc/'+name+
            '/mountinfo 2>/dev/null ', None, 'mountinfo',))
        self.collectors.append(Capability('capability', 'getpcaps '+name+' 2>&1 | cut -d \' \' -f 5-'))

class Container_Checker(Returnvalue_Checker):
    pass

class Contained_Process_Checker(Collector):
    def __init__(self, name):
        super(Contained_Process_Checker, self).__init__(name)
        rootDir = get_global_root() + '/proc'
        self.container_checker = Container_Checker('checker', './tools/container_ns.sh')
        try:
            for dirName in os.listdir(rootDir):
                if(re.match('[0-9]+', dirName)):
                    debug_print("checking pid: %s"%dirName)
                    if(self.container_checker.run(dirName)):
                        self.collectors.append(Individual_Proc_Info(dirName))
        except OSError as err:
            print err
class Containers_Installed(Collector):
    def __init__(self, name):
        super(Containers_Installed, self).__init__(name)
        pass

class Linux_Container_Checker(Collector):
    def __init__(self):
        super(Linux_Container_Checker, self).__init__('container')
        self.collectors.append(Contained_Process_Checker('contained_process'))
        self.collectors.append(Containers_Installed('container_software'))

