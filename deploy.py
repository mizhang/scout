#!/usr/bin/python
import os
import sys
from basic import *
from info_center import *
class Deploy_Maker:
	def __init__(self):
		pass
	def add_privilege(self,op, cmd):
		if(op.args['privilege']):
			return "sudo "+cmd
		else:
			return cmd

	def run(self, op):
		if(op.mode == 'deploy'):
			cmdline = "docker pull %s"%op.args['image_name']
			cmdline=self.add_privilege(op, cmdline)
			debug_print("command line is: %s"%cmdline)
			os.system(cmdline)
			return
		elif(op.mode == 'run'):
			cmdline ="docker run %s %s %s"%(op.args['interactive'], op.args['image_name'], ' '.join(op.args['docker_cmd']))
			cmdline=self.add_privilege(op, cmdline)
			debug_print("command line is %s"%cmdline)
			os.system(cmdline)
			return
		else:
			print("operation: %s, not implemented"%op.mode)
			sys.exit(0)
