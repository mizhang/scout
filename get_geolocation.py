#!/usr/bin/python
import urllib
from urllib import urlopen
import re
import os
import pygeoip
from info_center import Info_Center
from basic import *
class External_Access_Checker(Collector):
    def __init__(self, name, public_ip):
        super(External_Access_Checker, self).__init__(name)
        self.public_ip = public_ip
        self.config = './config/server_ip'
        with open(self.config, 'r') as f:
            self.server_ip = f.read().rstrip()
        debug_print("server ip: %s"%self.server_ip)

        self.collectors.append(External_Access("bash", ":",
            "./tools/network_access_client.sh " + self.public_ip + ' '+
            self.server_ip, None, 'port_accessable'))

    def send_info(self, recon_info):
        recon_info[self.getname()] = dict()
        super(External_Access_Checker, self).send_info(recon_info)
        recon_info[self.getname()]['public_ip'] = self.public_ip
        recon_info[self.getname()]['accessable'] = "no"
        for collector in self.collectors:
            if(collector.getname() == "port_accessable"):
                ports = collector.data
                can_access = False
                for key in ports.keys():
                    if(ports[key] == "succeed"):
                        can_access = True
                        break
                if(can_access):
                    recon_info[self.getname()]['accessable'] = "yes"
                    return


class External_Access(Keyvalue_Script):
    pass

class Access_Internet_Checker:
    def __init__(self):
        self.config = "./config/access_sites"
        self.to_internet = 1;

    def access_site(self, url):
        #print(urlopen(url).getcode();)
        try:
            response = urlopen(url)
            return True;
        except:
            print("error openning URL: %s"%url)
            return False;
    def run(self):
        with open(self.config) as f:
            sites = f.readlines()
        for site in sites:
            site = site.rstrip()
            if(self.access_site(site) == False):
                print("accessing site: %s is NOT accessible"%site)
                self.to_internet = 0
                break;
            else:
                print("checking internet accessibility: %s is accessible"%site)
        return self.to_internet;


class Geo_Location:
    def __init__(self):
        self.public_ip = "";
        #free database: http://dev.maxmind.com/geoip/legacy/geolite/
        self.geo_handler = pygeoip.GeoIP('./config/GeoLiteCity.dat')
        self.external_access = None
        self.geo_info = None;
        pass
    def getname(self):
        return 'geolocation'
    def get_public_ip(self):
        return urllib.urlopen('http://ip.42.pl/raw').read();
    def get_geolocation(self, ip):
        self.geo_info = self.geo_handler.record_by_addr(ip)
    def print_geo_location(self):
        debug_print("geolocation is:")
        debug_print(self.geo_info);

    def run(self):
        access = Access_Internet_Checker()
        if(access.run() == 0):
            print("No full Internet Access")
            return 0;
        self.public_ip = self.get_public_ip()
        self.external_access = External_Access_Checker('external_access', self.get_public_ip())
        #self.external_access = External_Access_Checker('external_access', 'localhost')
        self.external_access.run()
        self.get_geolocation(self.public_ip);
        self.print_geo_location();
        return 1;
    def send_info(self, recon_info):
        recon_info[self.getname()] = self.geo_info;
        if(self.external_access != None):
            self.external_access.send_info(recon_info)


