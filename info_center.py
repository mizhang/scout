#!/usr/bin/python
import json
from basic import *
class Operation:
    def __init__(self,mode=None, args=dict()):
        self.mode = mode
        self.args = args;

class Info_Center:
    def __init__(self):
        self.all_info = dict();
        self.all_info['machine'] = dict()
        self.all_info['machine']['performance'] = dict()
        self.all_info['cloud_provider'] =dict()
        self.all_info['security'] = dict();
        self.all_info['network'] = dict();
        self.all_info['network']['geolocation'] = dict();
        self.all_info['network']['localnetwork'] = dict();
        self.all_info['network']['cloudprovider'] = dict();
    def dump(self, name='/tmp/scout_info.dat'):
        debug_print("============================")
        debug_print("scout information as follows:")
        with open(name, 'w') as outfile:
            json.dump(self.all_info, outfile)
        #debug_print(json.dumps(self.all_info, indent=4))
        debug_print("============================")


