#!/bin/bash
if [[ $EUID -ne 0 ]]; then
    echo "Please run this script as root. "
    exit 0
fi
if [[ $# -ne 1 ]]; then
    echo "Usage: vanguard_monitor.sh <image>"
    exit 0
fi
IMAGE_NAME=$1
CUR_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"


#kill existing processes and container
docker kill vanguard-scout 2>/dev/null
#kill vanguard-daemon separately because it does not belong to any container
for pid_file in /tmp/vanguard/[0-9]*
do
    pid=`basename $pid_file`
    echo "killing running vanguard scout daemon $pid"
    kill -9 $pid 2>/dev/null
    rm $pid_file
done
docker rm vanguard-scout 2>/dev/null

#launch the vanguard-daemon container 
docker run --privileged `$CUR_DIR/mount_host.sh` --name vanguard-scout -d $IMAGE_NAME

#start collecting information by launching the daemon 
$CUR_DIR/docker-native-enter vanguard-scout "/scout/vanguard-daemon.sh /host/" >/dev/null 2>&1 &
echo $!
exit
