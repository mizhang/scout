#!/bin/bash
#kill existing processes and container
if [[ $EUID -ne 0 ]]; then
    echo "Please run this script as root. "
    exit 0
fi

docker kill vanguard-scout 2>/dev/null
#kill vanguard-daemon separately because it does not belong to any container
for pid_file in /tmp/vanguard/[0-9]*
do
    pid=`basename $pid_file`
    echo "killing running vanguard scout daemon $pid"
    kill -9 $pid 2>/dev/null
    rm -f $pid_file 2>/dev/null
done
docker rm vanguard-scout 2>/dev/null

