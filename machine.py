#/usr/bin/python
from basic import *
import re
import platform
class Performance_Metric(Collector):
    def __init__(self, name):
        super(Performance_Metric, self).__init__(name)
        self.collectors.append(CPU("", ":", "lscpu", CPU_Preprocessor(), 'cpu'))
        self.collectors.append(Memory('', None,
            'free -m|head -n 2| cut -d " " -f 2- |sed \'s/^\s*//g\'', None,
            'mem'))

class CPU_Preprocessor(Preprocessor):
    def process_keyvalue(self, key, value):
        key = re.sub(' ', '_', key)
        key = key.lower()
        key = re.sub("\(s\)",'_num', key)
        key = re.sub('-', '_', key)
        value = value.strip()
        return (key, value)

class CPU(Keyvalue_Script):
    pass

class OS_Distrib(Basic_Object):
    def run(self):
        self.data = dict()
        self.data['name'] = platform.linux_distribution()[0]
        self.data['detail'] = platform.dist()
        self.data['machine'] = platform.machine()
        self.data['platform'] = platform.platform()
        self.data['version'] = platform.version()
    pass
class Kernel_Version(Script_Object):
    pass
class Kernel_Aslr(Script_Object):
    pass
class Kernel_Ptrace(Script_Object):
    pass

class Memory(Table_Script):
    def send_info(self, recon_info):
        #XXX: assume there is only one memory bank here
        recon_info[self.getname()] = self.get_data_elem(0);

# This class contains file system info of docker images
class File_System_Info(Table_Script):
    def send_info(self, recon_info):
        recon_info[self.getname()] = self.get_data_elem(0)

class Kernel(Collector):
     def __init__(self, name):
        super(Kernel, self).__init__(name)
        self.collectors.append(Kernel_Version('version', 'uname -r'))
        self.collectors.append(Kernel_Aslr('aslr', 'cat '+get_global_root()+'proc/sys/kernel/randomize_va_space'))
        self.collectors.append(Kernel_Ptrace('ptrace', 'cat '+get_global_root()+'proc/sys/kernel/yama/ptrace_scope'))

class OS(Collector):
    def __init__(self, name):
        super(OS, self).__init__(name)
        self.collectors.append(OS_Distrib('distrib'))
        self.collectors.append(Kernel('kernel'))
        self.collectors.append(File_System_Info('',None,
            'df -P -T /var/lib/docker | sed \'s/Mounted on/mount_point/g\'',
            None, 'docker_fs'));

class Machine_Info(Collector):
    def __init__(self):
        super(Machine_Info, self).__init__('machine')
        self.collectors.append(OS('os'))
        self.collectors.append(Performance_Metric('performance'))

