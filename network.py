#!/usr/bin/python
from basic import *
from get_geolocation import Geo_Location

class Net_State(Table_Script):
    def process_meta_line(self, proc):
        if(self.attributes != None):
            return
        attr_line = proc.stdout.readline().rstrip()
        debug_print("attr line: %s"%attr_line)
        self.remove_index = attr_line.index('State')

        if(self.remove_index != -1):
            self.remove_length = 6
            attr_line = attr_line.replace('State', '')
            debug_print("attr line: %s"%attr_line)

        self.attributes = self.split(attr_line)

    def process_data_line(self, line):
        if(self.remove_index != None and self.remove_index != -1):
            line = line[:self.remove_index-1] +\
            line[self.remove_index+self.remove_length:]
        debug_print("data line: %s"%line)
        super(Net_State, self).process_data_line(line)


class Local_Services(Basic_Object):
    pass

class Link_Interface(Keyvalue_Script):
    pass

class Inet_Interface(Keyvalue_Script):
    pass
class Link_Status(Table_Script):
    def send_info(self, recon_info):
        recon_info[self.getname()] = self.get_data_elem(0)

class Network_Interface(Collector):
    def __init__(self, name):
        super(Network_Interface, self).__init__(name)
        self.collectors.append(Inet_Interface('',":", 'ifconfig '+
                        self.name+ '| grep \'inet addr\'|\
                        sed \'s/^\s*inet addr/inet_addr/g; s/\s\+/\\n/g\''
                        , None, 'inet'))
        self.collectors.append(Link_Interface('',";", 'ifconfig '+
                        self.name+ '|head -1|\
                        sed \'s/.*Link encap:/link_encap;/g;\
                        s/HWaddr\s*/hwaddr;/g; s/\s\s\+/\\n/g\'|\
                        sed \'/^\s*$/d\'', None, 'link_net'))
        self.collectors.append(Link_Status('', None, 'ifconfig -s '+ self.name,
                        None, 'link_status'))

class Local_Network(Dynamic_Collector):
    def init_collector(self, c):
       return Network_Interface(c)

class Network_Info(Collector):
    def __init__(self):
        super(Network_Info, self).__init__('network');
        self.collectors.append(Geo_Location())
        self.collectors.append(Net_State('', None, 'netstat -ntupl 2>/dev/null|\
                                    sed \'s/Local Address/local_address/g\'|\
                                    sed \'s/Foreign Address/foreign_address/g\'|\
                                    sed \'s/PID\/Program name/pid_program_name/g\'|\
                                    tail -n +2', None, 'netstat'))
        self.collectors.append(Local_Network('', Data_Type.raw_data, None,
                                    'ifconfig |\
                                    sed \'/^\s\+/d; /^\s*$/d; \'|\
                                    awk \'{print $1 }\'',
                                    None, 'localnetwork'))
    def send_info(self, recon_info):
        super(Network_Info, self).send_info(recon_info)


