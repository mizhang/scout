#!/usr/bin/python
import os
import sys
import json
from info_center import *
class Policy:
    def __init__(self, file):
        self.scout_info = None
        self.json_handler = open(file)
        try:
            self.policy_data = json.load(self.json_handler)
        except ValueError as err:
            print(err)
            print("policy file: [%s] is not in JSON format"%file)
            self.policy_data = self.create_empty_policy()

    def get_image_name(self):
        if(isinstance(self.policy_data, dict)):
            return self.policy_data['image_name']
        return ""

    def create_empty_policy(self):
        policy_data = dict()
        policy_data['image_name'] = 'null'
        policy_data['rules'] =dict()
        policy_data['precondition'] = dict()
        policy_data['decision'] = dict()
        return policy_data

    def is_empty_policy(self):
        if(self.policy_data['image_name'] == 'null'):
            return True;
        else:
            return False;

    def satisfy(self, recon_info):
        rules = self.policy_data['rules']
        return self.check_rule(rules, recon_info)

    def ismatch(self, value1, value2):
        if(value1 == value2):
            return True
        else:
            return False

    def isoneof(self, value1, value2):
        if(value2.find(value1) == -1):
            return False
        else:
            return True
    def check_rule(self, rule, recon_info):
        if(isinstance(rule, dict) == False or isinstance(recon_info, dict) == False):
            print("malformed policy file or recon_info")
            return False

        if(len(rule.keys()) == 0 or len(recon_info.keys()) == 0):
            print("dictionary, either 'rule' or 'recon_info' is empty")
            return False
        for key in rule:
            #debug_print("rule:")
            #debug_print(rule[key])
            if(key not in recon_info.keys()):
                print("cannot find key: %s in recon_info"%key)
                return False

            if(isinstance(rule[key], dict) and 'condition' in rule[key].keys()):
                if(rule[key]['condition'] == 'match'):
                    if('value' not in rule[key].keys()):
                        print("malformed policy file: no 'value' keypair in file")
                        return False
                    if(self.ismatch(recon_info[key], rule[key]['value']) == False):
                        print("policy mismatch:")
                        print(rule[key]['value'])
                        print(recon_info[key])
                        return False
                elif(rule[key]['condition'] == 'isoneof'):
                    if('value' not in rule[key].keys()):
                        print("malformed policy file: no 'value' keypair in file")
                        return False
                    if(self.isoneof(recon_info[key], rule[key]['value']) == False):
                        print("policy mismatch:")
                        print(rule[key]['value'])
                        print(recon_info[key])
                        return False
                else:
                    print("malformed policy file: unrecognized condition option")
                    return False
            elif(self.check_rule(rule[key], recon_info[key]) == False):
                return False
        return True

class Policy_Checker:
    def __init__(self):
        self.policies = []
        self.register_policies()
        self.decision = {}
        self.decision['deploy'] = 0
        self.decision['image_path'] = ""
        self.operation = None;
    def register_policies(self):
        policy_dir = './policies'
        for file in os.listdir(policy_dir):
            if(file.endswith(".policy")):
                policy = Policy(policy_dir+"/"+file)
                self.policies.append(policy)
        if(self.no_valid_policy()):
            print("no valid policy loaded, exit")
            sys.exit(1);

    def no_valid_policy(self):
        for policy in self.policies:
            if(policy.is_empty_policy() == False):
                return False
        return True
    def matches(self, image_in_policy, image_to_check):
        #so far, it is a simple check
        if(image_in_policy == image_to_check):
            return True

        return False

    def check_if_policy_apply(self):
        policies = []
        for policy in self.policies:
            if(self.matches(policy.get_image_name(), self.operation.args['image_name'])):
                print("load policy for image: %s"%policy.get_image_name())
                policies.append(policy)
        return policies

    def run(self, recon_info, operation):
        self.operation = operation
        self.scout_info = recon_info
        policies = self.check_if_policy_apply()
        if(policies == None or (not isinstance(policies, list)) or len(policies) == 0):
            print("no policy has been selected, do not deploy")
            self.decision['decision'] = 0
            return

        for policy in policies:
            if(policy.satisfy(recon_info) == 0):
                print("policy does not satisfy")
                self.decision['deploy'] = 0
                return
        self.decision['deploy'] = 1

    def if_deploy(self):
        return self.decision['deploy']
