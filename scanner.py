#!/usr/bin/python
import sys

from basic import *
from network import Network_Info
from info_center import Info_Center
from cloudprovider import Cloud_Checker
from security import Security_Checker
from machine import Machine_Info
from container import *
class Scanner(Super_Collector):
    def __init__(self):
        super(Scanner, self).__init__('scanner')
        self.collectors.append(Security_Checker())
        self.collectors.append(Machine_Info())
        self.collectors.append(Network_Info())
        self.collectors.append(Cloud_Checker())
        self.collectors.append(Linux_Container_Checker())

