#!/usr/bin/python
from scanner import *
from policy import Policy_Checker
from info_center import *
from deploy import Deploy_Maker
import sys
from basic import *
import argparse
global global_root_dir
def parse_arg(args):
    #debug_print(args;)
    description="Symantec Super-Container Launcher"
    parser = argparse.ArgumentParser(description=description)
    sp = parser.add_subparsers(dest="subparser_name");
    sp_deploy = sp.add_parser('deploy', help="deploy a docker image")
    sp_scout = sp.add_parser('scout', help="scout the running environment")
    sp_scout.add_argument('--path', action="store", default="/")
    sp_scout.add_argument('-o', '--output', action="store", default="/tmp/scout_info.dat")
    sp_run = sp.add_parser('run', help="run a docker image")
    sp_deploy.add_argument('image')
    sp_deploy.add_argument('--privilege', action="store_true")
    sp_run.add_argument('image', type=str)
    sp_run.add_argument('-i', '--interactive', action="store_true")
    sp_run.add_argument('--privilege', action="store_true")
    sp_run.add_argument('-c', '--command', nargs='*')
    args = parser.parse_args()
    #debug_print(args)
    op = None;
    if(args.subparser_name == "deploy"):
        op = Operation('deploy')
        op.args['image_name'] = args.image;
        op.args['privilege'] = args.privilege;
        debug_print("image to deploy is : %s"%op.args['image_name'])
    elif(args.subparser_name == "run"):
        op = Operation('run')
        op.args['image_name'] = args.image
        op.args['privilege'] = args.privilege;
        op.args['docker_cmd'] = args.command
        if(args.interactive):
            op.args['interactive'] = "-i -t"
        else:
            op.args['interactive'] = " -d "
        debug_print("image to run is : %s"%op.args['image_name'])
    elif(args.subparser_name == "scout"):
        op = Operation('scout')
        if(args.path != None):
            op.args['path'] = args.path
        else:
            op.args['path'] = "/"
        op.args['output'] = args.output
        print("scout path: %s"%op.args['path'])
        set_global_root(op.args['path'])

    return op;

if __name__ == "__main__":
    global_init()
    config = Global_Config()
    op = parse_arg(sys.argv);
    #initializing all objects
    scanner = Scanner();
    info_center = Info_Center();
    policy_checker = Policy_Checker();
    deploy_maker = Deploy_Maker();

    #scanning information of the machine
    scanner.run();

    scanner.send_info(info_center.all_info)
    #debug_print(info_center.all_info)
    #send the information to the policy checker
    info_center.dump(op.args['output'])

    if(op.mode == 'scout'):
        sys.exit(0)
    policy_checker.run(info_center.all_info, op)
    #get decision back and retrieve docker image
    if(policy_checker.if_deploy() == 0):
        debug_print("policy not satisfied, do not deploy")
        sys.exit(0)
    else:
        debug_print("policy satisfied !")

    #deploy the docker container if policy is satisfied
    deploy_maker.run(op);
