#!/usr/bin/python
import re
from basic import *
#preprocessor classes to handle format issues
class Checksec_Kernel(Preprocessor):
    def __init__(self):
        super(Checksec_Kernel, self).__init__()
        pass
    def process_keyvalue(self, key, value):
        key = re.sub(' ', '_', key)
        key = key.lower()
        value = value.strip()
        if(re.match("No ", value)):
            value = "no"
        elif(re.match("Disabled", value)):
            value = "no"
        elif(re.match("Enabled", value)):
            value = "yes"
        return (key, value)

class Checksec_Proc(Preprocessor):
    def __init__(self):
        super(Checksec_Proc, self).__init__()
        pass
    def process_attr_line(self, attrs):
        for i in range(len(attrs)):
            if(attrs[i] == ""):
                continue
            #print "attr[%d]: %s"%(i, attrs[i])
            tmp = attrs[i].lower()
            tmp = re.sub(' ', '_', tmp)
            if(tmp == "command"):
                tmp = "name"
            attrs[i] = tmp
        return attrs
    def process_data_line(self, fields):
        return fields




class Security_Checker(Collector):
    def __init__(self):
        super(Security_Checker, self).__init__('security')
        self.collectors.append(Keyvalue_Script("bash", ":", "./tools/checksec/check_kernel.sh", Checksec_Kernel(), 'pax_kernel'))
        self.collectors.append(Table_Script("bash", "\t", "./tools/checksec/check_proc.sh", Checksec_Proc(), 'pax_proc'))

    def send_info(self, recon_info):
        recon_info[self.getname()] = dict()
        for col in self.collectors:
            col.send_info(recon_info[self.getname()])


if __name__ == "__main__":
    sec_checker = Security_Checker()
    sec_checker.run()
