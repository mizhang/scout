#!/bin/bash
host=$1
keyfile=$2

for user in root ec2-user ubuntu admin bitnami
do
  if timeout 5 ssh -i $keyfile $user@$host true 2>/dev/null; then
    echo "ssh -i $keyfile $user@$host"
  fi
done
