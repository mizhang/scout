#!/bin/bash
native=`ls -l /proc/self/ns 2>/dev/null | awk '{print $11}' `
target=`ls -l /proc/$1/ns  2>/dev/null| awk '{print $11}'`

#echo "native:" $native
#echo "target:" $target
if [ -z "$target" ]; then
    exit 1
fi
if [ "$native" == "$target" ]; then
    exit 1
else
    exit 0
fi
