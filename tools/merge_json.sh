#!/bin/bash
if [ $# != 2 ]; then
    echo "<program> <input_dir> <output_file>"
    exit
fi
input_dir=$1
output_file=$2
echo -e "{ \"sample\": {}" >$output_file; 
for f in $input_dir/*.dat
do 
    key=`basename $f`
    key="${key%.*}"; 
    key=`echo "$key" |tr '.@' '_'`; 
    echo $key
    #sed -n 's/{\(.*\)}/,\n'\"$key\"' : {\1}/gp' $f >>$output_file;
    sed -n "s/{\(.*\)}/,\n \"$key\" : {\1}/gp" $f >>$output_file;
done
echo }>>$output_file
