#!/bin/bash
#use some imtest_port test_port to test
path=`dirname $(readlink -f $0)`
if [ $# != 2 ]; then
	echo "executable <CLIENT IP> <SERVER IP>" 
	exit
fi
MAXCOUNT=3
count=1
CLIENT_IP=$1
SERVER_IP=$2
#randomly generate some test_port to test
while [ $count -le $MAXCOUNT ]
do
	test_port=$RANDOM
	#XXX: random port might be the same an already opened port
	let "test_port %= 64511" #65535 - 1024
	let "test_port += 1025"  #avoid generating privileged test_ports
	if $path/network_access_connect.sh $CLIENT_IP $test_port $SERVER_IP; then
		echo "$test_port:succeed"
	else
		echo "$test_port:fail"
	fi
	let "count = count+1"
done

