#!/bin/bash
TEST_IP=$1
TEST_PORT=$2
DEST_IP=$3
DEST_PORT=33767
#trying to connect and send the port number to open
echo "$TEST_IP $TEST_PORT"|nc $DEST_IP $DEST_PORT
#listen on port and check payload received
if payload=`timeout 3 nc -l $TEST_PORT`; then
	if [ $payload == "NetworkTesting" ]; then
		exit 0
	else
        echo "payload incorrect"
		exit 1
	fi
else
	exit 1
fi
