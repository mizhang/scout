#!/bin/bash
serve_request() {
    local ip_port=$1
	echo "connection from $ip_port"
    echo "sending payload"
    sleep 1
	echo NetworkTesting | timeout 1 nc  $ip_port
    echo "done"
}
while true;
do
    echo "listening on port 33767"
	ip_port=`nc -l 33767`
    serve_request "$ip_port" 
done
