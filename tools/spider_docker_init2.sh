#!/bin/bash
touch /tmp/docker/result
while read line
do
    echo "search $line in docker hub"
    docker search "$line" >>/tmp/docker/result
done < /tmp/docker_dict
