#!/bin/bash

USER_HOME=$(eval echo ~${SUDO_USER})
VANGUARD_PATH="/"
VANGUARD_SLEEP=10
VANGUARD_SCOUT_DIR="/tmp/vanguard"
VANGUARD_SCOUT_DATA=$VANGUARD_SCOUT_DIR"/scout_info.dat"
cleanup() {
   echo "vanguard cleanup..."
   echo "root pid is: "$ROOT_PID
   kill -9 $ROOT_PID
}

control_c() {
    echo "Ctrl-C is pressed, vanguard will exit..."
    cleanup    
    exit $?
}

trap control_c SIGINT

mount -t proc proc /proc
TERM=xterm
export TERM

if [[ $# == 0 ]]; then
    echo "no parameter entered, vanguard using default parameter..."
elif [[ $# == 1 ]]; then
    echo "vanguard scan interval will be $VANGUARD_SLEEP seconds"
    VANGUARD_PATH=$1
elif [[ $# == 2 ]]; then
    VANGUARD_PATH=$1
    VANGUARD_SLEEP=$2
fi
VANGUARD_SCOUT_DATA_RELEASE="$VANGUARD_PATH/$VANGUARD_SCOUT_DIR/scout_info_latest.dat"

mkdir -p $VANGUARD_SCOUT_DIR
mkdir -p "$VANGUARD_PATH/$VANGUARD_SCOUT_DIR"
cd /scout

rm "$VANGUARD_PATH/$VANGUARD_SCOUT_DIR/$$"
touch "$VANGUARD_PATH/$VANGUARD_SCOUT_DIR/$$"

while true;
do
    ./scout.py scout --path $VANGUARD_PATH -o $VANGUARD_SCOUT_DATA >/$VANGUARD_PATH/$VANGUARD_SCOUT_DIR/log 2>&1
    mv $VANGUARD_SCOUT_DATA $VANGUARD_SCOUT_DATA_RELEASE
    echo "vanguard will sleep $VANGUARD_SLEEP seconds"
    sleep $VANGUARD_SLEEP
done
